package com.zhangzz.crypto.enums;

public enum KeyPairAlgorithmId {

    SM2("SM2"),
    RSA("RSA"),
    ;

    private String alg;

    public String getAlg() {
        return alg;
    }

    KeyPairAlgorithmId(String alg){
        this.alg = alg;
    }
}
