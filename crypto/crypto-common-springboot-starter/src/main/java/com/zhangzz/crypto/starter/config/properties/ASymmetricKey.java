package com.zhangzz.crypto.starter.config.properties;

import com.zhangzz.crypto.enums.KeyPairAlgorithmId;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ASymmetricKey {

    /**
     * 非对称密钥对公钥，需和非对称加解密、签名算法相匹配
     */
    private String publicKey = "MFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAEPqoVGXuSdibdj8zgLTc8KwIAYufwtxPlljFBhAs3qm3+MjYI0ybIeOmpQkN1MU0PcoO8pVQprRWG6sTAw6qyTg==";

    /**
     * 非对称密钥对私钥，需和非对称加解密、签名算法相匹配
     */
    private String privateKey = "MIGTAgEAMBMGByqGSM49AgEGCCqBHM9VAYItBHkwdwIBAQQgeRsn2MLt4/r/14AeC3lEnC0W62kKj1zIGIIxaxc1r3mgCgYIKoEcz1UBgi2hRANCAAQ+qhUZe5J2Jt2PzOAtNzwrAgBi5/C3E+WWMUGECzeqbf4yNgjTJsh46alCQ3UxTQ9yg7ylVCmtFYbqxMDDqrJO";

    private String alg = KeyPairAlgorithmId.SM2.getAlg();

}
