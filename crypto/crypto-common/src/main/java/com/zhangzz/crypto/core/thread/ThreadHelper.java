package com.zhangzz.crypto.core.thread;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 线程帮助类
 * @author zhangzz
 */
public class ThreadHelper {

    private static volatile ThreadPoolExecutor executor = null;

    private static volatile ThreadHelper helper;

    private ThreadHelper(){

    }

    public static ThreadHelper newInstance(){
        if(executor == null){
            synchronized (ThreadHelper.class){
                if(executor == null){

                    int coreNum = Runtime.getRuntime().availableProcessors();
                    executor = new ThreadPoolExecutor(coreNum,2*coreNum,30000,
                            TimeUnit.MILLISECONDS,new LinkedBlockingQueue<>(),new CustomThreadFactory());
                }
            }
        }

        if(helper == null){
            synchronized (ThreadHelper.class){
                if(helper == null){

                    helper = new ThreadHelper();
                }
            }
        }
        return helper;
    }


    public ThreadPoolExecutor getThreadPoolExecutor(){

        return executor;
    }


    public void shutdown(){
        executor.shutdown();
    }

    public void shutdownNow(){
        executor.shutdownNow();
    }

}
