package com.zhangzz.crypto.enums;

public enum Padding {

    PKCS1Padding("PKCS1Padding"),
    PKCS5PADDING("PKCS5PADDING"),
    PKCS7PADDING("PKCS7PADDING"),
    NOPADDING("NOPADDING"),
    NONE(""),
    DEFAULT(null);

    private String padding;

    public String getPadding() {
        return padding;
    }

    Padding(String padding){
        this.padding = padding;
    }
}
