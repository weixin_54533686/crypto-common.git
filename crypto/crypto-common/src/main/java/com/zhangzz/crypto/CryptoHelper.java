package com.zhangzz.crypto;

import com.zhangzz.crypto.core.entity.ZKeyPair;
import com.zhangzz.crypto.core.exception.CryptoException;
import com.zhangzz.crypto.enums.KeyAlgorithmId;
import com.zhangzz.crypto.enums.KeyPairAlgorithmId;
import com.zhangzz.crypto.utils.CryptoUtil;
import org.bouncycastle.util.encoders.Base64;

/**
 * java -jar .\crypto-common-1.0-jar-with-dependencies.jar SM2 256
 */
public class CryptoHelper {

    public static void main(String[] args) {
        try {
            if(args.length != 2){
                throw new CryptoException("参数错误");
            }

            Integer keyLength = Integer.parseInt(args[1]);

            if(args[0].equals("SM4") || args[0].equals("AES") || args[0].equals("3DES")){

                byte[] key = CryptoUtil.generateKey(KeyAlgorithmId.SM4,keyLength);
                System.out.println(args[0]+"密钥："+Base64.toBase64String(key));
                return ;
            }

            ZKeyPair keyPair = null;
            if(args[0].equals("SM2")) {
                keyPair = CryptoUtil.generateKeyPair(KeyPairAlgorithmId.SM2, keyLength);

            }

            if(args[0].equals("RSA")) {
                keyPair = CryptoUtil.generateKeyPair(KeyPairAlgorithmId.RSA, keyLength);
            }

            System.out.println("公钥:"+Base64.toBase64String(keyPair.getPublicKey()));
            System.out.println("私钥："+Base64.toBase64String(keyPair.getPrivateKey()));

        } catch (CryptoException e) {
            e.printStackTrace();
        }
    }
}
