package com.zhangzz.crypto.core.exception;

public class CommonException extends RuntimeException{

    private static final long serialVersionUID = 4L;

    public CommonException(String msg, Throwable e) {
        super(msg, e);
    };

    public CommonException(String msg) {
        super(msg);
    };
}
