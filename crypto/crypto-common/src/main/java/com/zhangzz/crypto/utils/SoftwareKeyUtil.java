package com.zhangzz.crypto.utils;

import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class SoftwareKeyUtil {

    static{
        if(Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }
    /**
     * 字符串转私钥对象
     * @param privateKeyBytes 私钥
     * @param algorithm 非对称算法标识
     * @return 私钥对象
     */
    public static PrivateKey bytesToPrivateKey(byte[] privateKeyBytes, String algorithm){
    	if(algorithm.equals("SM2")){

            algorithm = "EC";
        }

        try {
            PKCS8EncodedKeySpec peks = new PKCS8EncodedKeySpec(privateKeyBytes);
            KeyFactory kf = KeyFactory.getInstance(algorithm, BouncyCastleProvider.PROVIDER_NAME);
            return kf.generatePrivate(peks);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("未知算法："+e.getMessage(),e);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException("无效私钥"+e.getMessage(),e);
        } catch (NoSuchProviderException e) {
            throw new RuntimeException("未找到算法提供商:"+e.getMessage(),e);
        }
    }

    /**
     * 字符串转公钥对象
     * @param publicKeyBytes 公钥
     * @param algorithm 非对称算法标识
     * @return 公钥对象
     */
    public static PublicKey bytesToPublicKey(byte[] publicKeyBytes, String algorithm){
    	if(algorithm.equals("SM2")){

            algorithm = "EC";
        }
        try {
            X509EncodedKeySpec eks = new X509EncodedKeySpec(publicKeyBytes);
            KeyFactory kf = KeyFactory.getInstance(algorithm,BouncyCastleProvider.PROVIDER_NAME);
            return  kf.generatePublic(eks);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("未知算法："+e.getMessage(),e);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException("无效公钥"+e.getMessage(),e);
        } catch (NoSuchProviderException e) {
            throw new RuntimeException("未找到算法提供商:"+e.getMessage(),e);
        }
    }

    /**
     * 获取对称密钥
     * @param key 对称密钥
     * @param algorithm 对称密钥算法标识
     * @return 对称密钥
     */
    public static SecretKey bytesToSecretkey(byte[] key, String algorithm){

        return  new SecretKeySpec(key,0,key.length,algorithm);
    }


    /**
     * 通过公钥信息获取公钥
     * @param publicKeyInfno 公钥信息
     * @return 公钥
     * @throws IOException 算法标识无法确认
     */
    public static PublicKey getPublicKey(SubjectPublicKeyInfo publicKeyInfno) throws IOException {
		return BouncyCastleProvider.getPublicKey(publicKeyInfno);
	}
    
    /**
     * 通过私钥信息获取私钥
     * @param privateKeyInfo 私钥信息
     * @return 私钥
     * @throws IOException 算法标识无法确认
     */
    public static PrivateKey getPrivateKey(PrivateKeyInfo privateKeyInfo) throws IOException {
		return BouncyCastleProvider.getPrivateKey(privateKeyInfo);
	}

}
