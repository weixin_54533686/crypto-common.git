package com.zhangzz.crypto.utils;

import com.zhangzz.crypto.core.exception.CommonException;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.*;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.cert.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 证书文件工具类，包含将证书文件或证书字符串转换为证书对象
 */
public class CertFileUtil {

    static {
        if(Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    /**
     * Base64编码的证书字符串转证书对象
     * @param base64EncodeCert Base64编码的证书字符串
     * @return 证书对象
     */
    public static X509Certificate getCertificate(String base64EncodeCert){
        InputStream input = null;
        try {
            if(!base64EncodeCert.contains("BEGIN CERTIFICATE")) {

                base64EncodeCert = "-----BEGIN CERTIFICATE-----\n"+base64EncodeCert+"\n-----END CERTIFICATE-----";
            }
            input = new ByteArrayInputStream(base64EncodeCert.getBytes());

            return readCert(input);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);

        } finally {

            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    throw new RuntimeException("证书字符串输入流关闭失败，原因："+e.getMessage(),e);
                }
            }
        }
    }

    /**
     * 证书文件转为证书对象
     * @param file 证书文件
     * @return 证书对象
     * @throws IOException 证书文件读取发生IO异常
     */
    public static X509Certificate getCertificate(File file) {
        InputStream input = null;
        try {
            input = new FileInputStream(file);

            return readCert(input);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        } finally {

            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 证书文件转为证书对象
     * @param cert DER编码的证书字节数组对象
     * @return 证书对象
     * @throws IOException 证书文件读取发生IO异常
     */
    public static X509Certificate getCertificate(byte[] cert) throws IOException {
        InputStream input = null;
        try {
            input = new ByteArrayInputStream(cert);

            return readCert(input);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        } finally {

            if (input != null) {
                input.close();
            }
        }
    }

    private static X509Certificate readCert(InputStream input) throws CertificateException, NoSuchProviderException {
        CertificateFactory cf = CertificateFactory.getInstance("X.509", BouncyCastleProvider.PROVIDER_NAME);
        X509Certificate cert = (X509Certificate) cf.generateCertificate(input);
        return cert;
    }

    /**
     * CRL字符串转换为CRL对象
     * @param crlContent CRL字符串
     * @return X509CRL对象
     */
    public static X509CRL getCrl(String crlContent){
        /** 证书撤消列表格式 BEGIN */
        String BEGIN_X509_CRL = "-----BEGIN X509 CRL-----\n";

        /** 证书撤消列表格式 END */
        String END_X509_CRL = "\n-----END X509 CRL-----\n";

        if (!crlContent.startsWith("-----BEGIN X509 CRL-----")) {
            crlContent = BEGIN_X509_CRL + crlContent;
        }

        if (!crlContent.endsWith("-----END X509 CRL-----")) {
            crlContent = crlContent + END_X509_CRL;
        }

        try (ByteArrayInputStream bis = new ByteArrayInputStream(
                crlContent.getBytes())){
            CertificateFactory cf = CertificateFactory.getInstance("X.509",BouncyCastleProvider.PROVIDER_NAME);
            return (X509CRL) cf.generateCRL(bis);
        } catch (CertificateException e) {
            e.printStackTrace();
            throw new CommonException("实例化【CertificateFactory】对象失败，原因："+e.getMessage(),e);
        }  catch (CRLException e) {
            e.printStackTrace();
            throw new CommonException("CRL读取异常，原因："+e.getMessage(),e);
        }catch(NoSuchProviderException e){
            e.printStackTrace();
            throw new CommonException("未找到算法提供商，请在静态块中引入【BouncyCastleProvider】:"+e.getMessage(),e);
        }catch (IOException e){
            e.printStackTrace();
            throw new CommonException("文件操作过程中，出现IO异常:"+e.getMessage(),e);
        }
    }

    /**
     * CRL字符串转换为CRL对象
     * @param crlContent CRL字符串
     * @return X509CRL对象
     */
    public static X509CRL getCrl(byte[] crlContent){

        try (ByteArrayInputStream bis = new ByteArrayInputStream(crlContent)){
            CertificateFactory cf = CertificateFactory.getInstance("X.509",BouncyCastleProvider.PROVIDER_NAME);
            return (X509CRL) cf.generateCRL(bis);
        } catch (CertificateException e) {
            e.printStackTrace();
            throw new CommonException("实例化【CertificateFactory】对象失败，原因："+e.getMessage(),e);
        }  catch (CRLException e) {
            e.printStackTrace();
            throw new CommonException("CRL读取异常，原因："+e.getMessage(),e);
        }catch(NoSuchProviderException e){
            e.printStackTrace();
            throw new CommonException("未找到算法提供商:"+e.getMessage(),e);
        }catch (IOException e){
            e.printStackTrace();
            throw new CommonException("文件操作过程中，出现IO异常:"+e.getMessage(),e);
        }
    }

    /**
     * CRL字符串转换为CRL对象
     * @param crl CRL文件
     * @return X509CRL对象
     */
    public static X509CRL getCrl(File crl){


        try(InputStream inputStream = new FileInputStream(crl)) {
            CertificateFactory cf = CertificateFactory.getInstance("X.509",BouncyCastleProvider.PROVIDER_NAME);
            return (X509CRL) cf.generateCRL(inputStream);
        } catch (CertificateException e) {
            e.printStackTrace();
            throw new CommonException("实例化【CertificateFactory】对象失败，原因："+e.getMessage(),e);
        }  catch (CRLException e) {
            e.printStackTrace();
            throw new CommonException("CRL读取异常，原因："+e.getMessage(),e);
        }catch(NoSuchProviderException e){
            e.printStackTrace();
            throw new CommonException("未找到算法提供商:"+e.getMessage(),e);
        }catch (FileNotFoundException e){
            throw new CommonException("未找到吊销列表文件:"+e.getMessage(),e);
        }catch (IOException e){
            e.printStackTrace();
            throw new CommonException("文件操作过程中，出现IO异常:"+e.getMessage(),e);
        }
    }

    /**
     * Base64编码的证书链字符串，转换为证书链对象
     * @param certPath Base64编码的证书链字符串
     * @return 证书链对象
     * @throws CertificateException 证书读取异常
     */
    public static CertPath getCertPath(File certPath){

        InputStream input = null;
        try {
            input = new FileInputStream(certPath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new CommonException("证书链文件不存在："+e.getMessage(),e);
        }
        CertificateFactory cf = null;
        try {
            cf = CertificateFactory.getInstance("X.509", BouncyCastleProvider.PROVIDER_NAME);
            return cf.generateCertPath(input,"PKCS7");
        } catch(NoSuchProviderException e){
            e.printStackTrace();
            throw new CommonException("未找到算法提供商："+e.getMessage(),e);
        }catch (CertificateException e){
            e.printStackTrace();
            throw new CommonException("实例化工厂失败："+e.getMessage(),e);
        }finally {
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 获取证书中的CRL颁发点
     * @param certificate 数字证书
     * @return CRL颁发点的http地址
     */
    public static List<String> getCrlByCert(X509Certificate certificate){

        byte[] crlPoint = certificate.getExtensionValue(Extension.cRLDistributionPoints.getId());
        if (crlPoint == null) {//判断数字证书是否有吊销列表发布点的属性
            return null;
        } else {
            ASN1Primitive asn1Primitive = null;
            //将吊销列表转换为ASN1对象
            try {
                asn1Primitive = ASN1Primitive.fromByteArray(crlPoint);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //将ASN1对象转换为ASN1的字符欻类型
            ASN1OctetString crlPointStr = DEROctetString.getInstance(asn1Primitive);
            ASN1Primitive crlPrimitive = null;
            try {
                crlPrimitive = ASN1Primitive.fromByteArray(crlPointStr.getOctets());
            } catch (IOException e) {
                e.printStackTrace();
            }

            //获取到crl发布点
            CRLDistPoint point = CRLDistPoint.getInstance(crlPrimitive);
            //发布点可以为多个，为了防止单个文件过大，可能会分多个URL对CRL文件分割存储。
            DistributionPoint[] distributionPoints = point.getDistributionPoints();
            List<String> crlUrlList = new ArrayList();

            //循环获取CRL发布地址
            for(int i = 0; i < distributionPoints.length; ++i) {
                DistributionPoint distributionPoint = distributionPoints[i];
                DistributionPointName pointName = distributionPoint.getDistributionPoint();
                if (pointName.getType() == 0) {
                    GeneralNames names = (GeneralNames)pointName.getName();
                    GeneralName[] nameArray = names.getNames();

                    for(int var21 = 0; var21 < nameArray.length; ++var21) {
                        GeneralName name = nameArray[var21];
                        if (name.getTagNo() == 6) {
                            crlUrlList.add(name.getName().toString());
                        }
                    }
                }
            }
            //此处为http的crl文件地址
            return crlUrlList;
        }

    }

    public static void main(String[] args) {
        X509Certificate certificate = CertFileUtil.getCertificate(new File("/home/zhangzz/Downloads/_.csdn.crt"));
        List<String> list = CertFileUtil.getCrlByCert(certificate);
        for (String crl:list){
            System.out.println(crl);
        }
    }
}
