package com.zhangzz.crypto.enums;

public enum DigestAlgorithm {

    SM3("SM3"),
    SHA256("SHA256"),
    SHA384("SHA384");

    private String value;

    public String getValue(){
        return value;
    }

    DigestAlgorithm(String value){
        this.value = value;
    }
}
