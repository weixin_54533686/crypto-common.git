package com.zhangzz.crypto.utils;

import com.zhangzz.crypto.core.exception.CommonException;
import com.zhangzz.crypto.core.exception.CryptoException;
import com.zhangzz.crypto.core.exception.IsRevokedException;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.DefaultAlgorithmNameFinder;

import java.io.*;
import java.security.*;
import java.security.cert.*;
import java.util.List;

/**
 * 证书相关的方法
 */
public class CertificateUtil {

    static {
        if(Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    /**
     * 验证用户证书的有效性，包括验证有效期、证书信任列表、吊销状态等
     *
     * @param certFile       用户证书
     * @param certChainFile  证书链文件
     * @param crlFile        证书吊销列表文件
     * @throws CertificateNotYetValidException 证书未生效
     * @throws CertificateExpiredException     证书已过期
     * @throws SignatureException              证书不被信任
     * @throws IsRevokedException 证书被吊销
     */
    public static void verifyCertificate(File certFile, File certChainFile, File crlFile) throws CertificateNotYetValidException, CertificateExpiredException,IsRevokedException {

        X509Certificate certificate = CertFileUtil.getCertificate(certFile);
        certificate.checkValidity();

        CertPath certPath = CertFileUtil.getCertPath(certChainFile);
        List<X509Certificate> certList = (List<X509Certificate>) certPath.getCertificates();
        boolean flag = false;
        for(X509Certificate caCert : certList) {
            if(certificate.getIssuerX500Principal().equals(caCert.getSubjectX500Principal())) {
                //使用机构证书验签用户证书
                flag = verify(caCert.getPublicKey(),certificate);
                if(flag) {
                    break;
                }
            }
        }

        if(!flag) {
            throw new CommonException("证书链验证证书签名失败");
        }
        X509CRL crlObj = CertFileUtil.getCrl(crlFile);
        if(crlObj.isRevoked(certificate)){
            X509CRLEntry crlEntry = crlObj.getRevokedCertificate(certificate);
            throw new IsRevokedException(crlEntry.getRevocationReason());
        }
    }

    /**
     * 验证用户证书的有效性，包括验证有效期、证书信任列表等
     *
     * @param certFile      base64编码的用户证书
     * @param certChainFile      证书链文件
     * @throws CertificateNotYetValidException 证书未生效
     * @throws CertificateExpiredException     证书已过期
     */
    public static void verifyCertificate(File certFile, File certChainFile) throws CertificateNotYetValidException, CertificateExpiredException {


        X509Certificate certificate = CertFileUtil.getCertificate(certFile);
        certificate.checkValidity();

        CertPath certPath = CertFileUtil.getCertPath(certChainFile);

        List<X509Certificate> certList = (List<X509Certificate>) certPath.getCertificates();
        boolean flag = false;
        for(X509Certificate caCert : certList) {
            //使用机构证书验签用户证书
            flag = verify(caCert.getPublicKey(),certificate);
            if(flag) {
                break;
            }
        }

        if(!flag) {
            throw new CommonException("证书链验证证书签名失败");
        }
    }

    private static boolean verify(PublicKey publicKey,X509Certificate certObj) {

        byte[] signValue = certObj.getSignature();
        byte[] data = null;
        try {
            data = certObj.getTBSCertificate();
        } catch (CertificateEncodingException e) {
            throw new CommonException("证书格式错误，未获取到证书内容，原因："+e.getMessage(),e);
        }
        AlgorithmIdentifier oid = new AlgorithmIdentifier(new ASN1ObjectIdentifier(certObj.getSigAlgOID()), DERNull.INSTANCE);
        String signName = new DefaultAlgorithmNameFinder().getAlgorithmName(oid);
        return verificationSignature(publicKey.getEncoded(), signName, signValue,data);
    }

    /**
     * 数字签名验签
     * @param publicKey 公钥（PKCS8编码的私钥字节数组，可参考非对称密钥对的生成接口返回结果。）
     * @param signAlg 签名算法
     * @param signValue 签名值
     * @param data 签名值的原文
     * @return true为验签通过，表示签名者是合法的。false为验证未通过，签名者为非法。
     */
    private static boolean verificationSignature(byte[] publicKey, String signAlg, byte[] signValue,byte[] data)throws CryptoException{

        PublicKey key = null;

        if(signAlg.endsWith("RSA")) {

            key = SoftwareKeyUtil.bytesToPublicKey(publicKey, "RSA");
        }else{
            key = SoftwareKeyUtil.bytesToPublicKey(publicKey, "SM2");
        }

        Signature inSignatue = null;
        try {
            inSignatue = Signature.getInstance(signAlg, BouncyCastleProvider.PROVIDER_NAME);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new CryptoException("未找到算法",e);
        }catch (NoSuchProviderException e){
            throw new CryptoException("未找到安全程序提供商，原因:"+e.getMessage(),e);
        }


        try {
            inSignatue.initVerify(key);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            throw new CryptoException("无效的公钥",e);
        }
        try {
            inSignatue.update(data);
        } catch (SignatureException e) {
            e.printStackTrace();
            throw new CryptoException("签名更新数据异常",e);
        }
        try {
            return inSignatue.verify(signValue);
        } catch (SignatureException e) {
            e.printStackTrace();
            throw new CryptoException("签名异常",e);
        }
    }

    /**
     * 将文件转换为字节数组
     * @param file 文件
     * @return 字节数组
     */
    public static byte[] getBytesByFile(File file){

        ByteArrayOutputStream result = null;
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
            byte[] buffer = new byte[1024];
            int length;
            result = new ByteArrayOutputStream();
            while ((length = inputStream.read(buffer)) != -1)
            {
                result.write(buffer, 0, length);
            }
            return result.toByteArray();
        } catch (IOException e) {

            e.printStackTrace();
            throw new CommonException("读取文件字节数组失败："+e.getMessage(),e);
        }finally {

            try {
                if(inputStream != null) {
                    inputStream.close();

                }
            } catch (IOException e) {
                e.printStackTrace();
                throw new CommonException("文件流关闭失败，原因："+e.getMessage(),e);
            }
        }
    }
}
