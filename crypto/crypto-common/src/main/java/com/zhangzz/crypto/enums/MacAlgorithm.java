package com.zhangzz.crypto.enums;

/**
 * MAC算法标识，包含HMAC和CMAC
 * @author zhangzz
 */
public enum MacAlgorithm {

    SM4CMAC("SM4CMAC"),
    AESCMAC("AESCMAC"),
    HMACSM3("HMACSM3"),
    HMACSHA256("HMACSHA256"),
    HMACSHA384("HMACSHA384"),
    DESedeCMAC("DESedeCMAC");

    private String value;

    public String getValue(){
        return value;
    }

    MacAlgorithm(String value){
        this.value = value;
    }

}
