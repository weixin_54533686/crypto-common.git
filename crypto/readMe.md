## 一、项目背景

&#x9;	鉴于很多人对于密码学的知识不是很熟悉，但又有这方面的需求，为了减少学习成本，特别推出该开发工具包。

&#x9;	再次声明，本开发包不适用于密码局的评测，满足规范的使用应当结合硬件对加密等密钥进行保护。

## 二、功能描述

正确的算法使用，一般是由各种算法组合使用，才可以达到机密性、完整性、抗抵赖性

1.  字符串的加解密（一般用于数据密文存储及传输）
2.  字符串的签名、验签（一般用于抗抵赖及完整性验证）
3.  获取字符串的摘要值（一般用于做完整性验证）
4.  对字符串进行mac运算（一般用于做消息认证）
5.  文件的加解密（文件的加解密采用了多线程处理，可支持大文件的加解密）

## 三、使用方法

### 1.添加依赖至工程中

```xml
<dependency>
    <groupId>com.zhangzz</groupId>
    <artifactId>crypto-common-springboot-starter</artifactId>
    <version>1.0</version>
</dependency>
```

### 2.通过spring依赖注入，将密码算法实现类注入

```java
//注入密码算法实现类
private Crypto crypto;

@Autowired
public void setCrypto(Crypto crypto) {
    this.crypto = crypto;
}
```

### 3.接口说明

```java
package com.zhangzz.crypto.starter.service;

import com.zhangzz.crypto.starter.config.properties.CryptoConfig;

import java.io.File;
import java.security.cert.X509Certificate;

/**
 * 密码应用接口
 * @author zhangzz
 * @date 2024-04-04
 */
public interface Crypto {

    /**
     * 字符串加密
     * @param data 待加密的字符串
     * @return Base64编码后的字符串密文
     */
    public String encrypt(String data);

    /**
     * 字符串解密
     * @param data 待解密的字符串密文
     * @return 返回字符串明文
     */
    public String decrypt(String data) ;

    /**
     * 对字符串做签名
     * @param data 待签名的字符串
     * @return 返回base64编码后的签名值
     */
    public String sign(String data);

    /**
     * 使用签名值和签名原文做验签，用于验证该签名值是否为该签名原文的签名值（可用于抗抵赖、完整性校验）
     * @param signValue 签名值
     * @param data 签名原文
     * @return
     */
    public boolean verificationSignature(String signValue,String data);

    /**
     * 使用签名值和签名原文做验签，用于验证该签名值是否为该签名原文的签名值（可用于抗抵赖、完整性校验）
     * @param certificate 数字证书（使用数字证书的公钥进行签名验证）
     * @param signValue 签名值
     * @param data 签名原文
     * @return
     */
    public boolean verificationSignature(X509Certificate certificate, String signValue, String data);

    /**
     * 获取数据的摘要值，默认摘要算法为：SM3
     * @param data
     * @return
     */
    public String getHash(String data);

    /**
     * mac运算，默认算法为HMACSM3，若为基于对称算法的mac运算，则使用与对称加解密的密钥
     * @param inData 数据原文
     * @return mac值
     */
    public String mac(String inData);

    /**
     * 文件加密，加密算法默认为：SM4
     * @param filePath 待加密文件路径
     * @param outFilePath 加密后的文件存储路径
     */
    public void encryptFile(String filePath, String outFilePath);

    /**
     * 文件加密，加密算法默认为：SM4
     * @param file 待加密文件
     * @param outFile 加密后的文件
     */
    public void encryptFile(File file, File outFile);

    /**
     * 文件解密
     * @param filePath 待解密文件路径
     * @param outFilePath 解密后的文件路径
     */
    public void decryptFile(String filePath, String outFilePath);

    /**
     * 文件解密
     * @param file 待解密的文件
     * @param outFile 解密后的文件
     */
    public void decryptFile(File file, File outFile);
    /**
     * 添加临时密钥及算法配置，仅当前线程有效。
     * @param config 配置信息
     */
    public void setTempConfig(CryptoConfig config);

}

```

### 4.进阶版使用

&#x9;	可通过yml，可对密钥、算法等重新配置，避免第三方恶意通过该工具包的默认配置进行暴力破解。以下为配置说明

配置项  配置说明  默认值 类型说明

| 配置项                                    | 配置说明                  | 默认值          | 参数说明                                                                 |
| :------------------------------------- | :-------------------- | :----------- | :------------------------------------------------------------------- |
| crypto.config.key                      | 128位的对称密钥             | 省略           | 16个字节的字节数组，base64编码后的字符串。                                            |
| crypto.config.encryptionAlgorithm      | 对称加密算法                | SM4/CBC      | 参考枚举：com.zhangzz.crypto.enums.Algorithm                              |
| crypto.config.aSymmetricKey.publicKey  | 公钥                    | 省略           | 参考下列非对称密钥代码生成示例                                                      |
| crypto.config.aSymmetricKey.privateKey | 私钥                    | 省略           | 同上                                                                   |
| crypto.config.aSymmetricKey.alg        | 非对称密钥对算法              | SM2          | 参考枚举：com.zhangzz.crypto.enums.KeyPairAlgorithmId                     |
| crypto.config.signAlgorithm            | 签名算法                  | SM3WithSM2   | 若公私钥的算法非SM2，请同步修改为相同关联算法，参考枚举：com.zhangzz.crypto.enums.SignAlgorithm |
| crypto.config.digestAlgorithm          | 摘要算法                  | SM3          | 摘要算法，参考枚举：com.zhangzz.crypto.enums.DigestAlgorithm                   |
| crypto.config.macAlgorithm             | MAC算法                 | HMACSM3      | MAC算法，参考枚举：com.zhangzz.crypto.enums.MacAlgorithm                     |
| crypto.config.padding                  | 对称算法的填充模式，若无特殊要求，无需修改 | PKCS7PADDING | 填充模式                                                                 |

yml配置示例如下：

```yml

crypto:
  config:
    macAlgorithm: HMACSHA256
```

非对称密钥对代码生成示例：

```java
ZKeyPair keyPair = CryptoUtil.generateKeyPair(KeyPairAlgorithmId.SM2,256); //此处为商密2号算法的生成示例，RSA的更换下算法和模长即可，
                                                                           //例如：CryptoUtil.generateKeyPair(KeyPairAlgorithmId.RSA,2048）
System.out.println(Base64.toBase64String(keyPair.getPublicKey()));
System.out.println(Base64.toBase64String(keyPair.getPrivateKey()));
```

###

