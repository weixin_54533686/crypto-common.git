package com.zhangzz.crypto.starter.service.impl;

import com.zhangzz.crypto.core.exception.IsRevokedException;
import com.zhangzz.crypto.starter.service.Certificate;
import com.zhangzz.crypto.utils.CertFileUtil;
import com.zhangzz.crypto.utils.CertificateUtil;

import java.io.File;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.List;

public class CertificateService implements Certificate {
    /**
     * 验证用户证书的有效性，包括验证有效期、证书信任列表、吊销状态等
     *
     * @param certFile      用户证书
     * @param certChainFile 证书链文件
     * @param crlFile       证书吊销列表文件
     */
    @Override
    public boolean verifyCertificate(File certFile, File certChainFile, File crlFile) {
        try {
            CertificateUtil.verifyCertificate(certFile,certChainFile,crlFile);
            return true;
        } catch (CertificateNotYetValidException e) {
            e.printStackTrace();
        } catch (CertificateExpiredException e) {
            e.printStackTrace();
        } catch (IsRevokedException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * 验证用户证书的有效性，包括验证有效期、证书信任列表等
     * @param certFile      base64编码的用户证书
     * @param certChainFile 证书链文件
     */
    @Override
    public boolean verifyCertificate(File certFile, File certChainFile) {
        try {
            CertificateUtil.verifyCertificate(certFile,certChainFile);
            return true;
        } catch (CertificateNotYetValidException e) {
            e.printStackTrace();
        } catch (CertificateExpiredException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * 获取证书中的CRL颁发点
     *
     * @param certificate 数字证书
     * @return CRL颁发点的http地址
     */
    @Override
    public List<String> getCrlByCert(X509Certificate certificate) {
        return CertFileUtil.getCrlByCert(certificate);
    }

    /**
     * 获取证书中的CRL颁发点
     *
     * @param certificate 数字证书
     * @return CRL颁发点的http地址
     */
    @Override
    public List<String> getCrlByCert(File certificate) {
        X509Certificate cert = CertFileUtil.getCertificate(certificate);
        return CertFileUtil.getCrlByCert(cert);
    }
}
