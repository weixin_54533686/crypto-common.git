package com.zhangzz.asn1.test;

import com.zhangzz.crypto.core.asn1.SM2Cipher;
import com.zhangzz.crypto.enums.Algorithm;
import com.zhangzz.crypto.enums.Padding;
import com.zhangzz.crypto.utils.ByteUtil;
import com.zhangzz.crypto.utils.CryptoUtil;
import org.bouncycastle.crypto.engines.SM2Engine;
import org.bouncycastle.crypto.params.ECDomainParameters;

import java.io.IOException;

/**
 * SM2Cipher转换的代码示例
 */
public class SM2CipherTest {

    public static void main(String[] args) {
        byte[] publicKey = null ; //正式使用时，请替换为SM2公钥
        //加密后的密文，默认密文结构为C1C3C2
        byte[] cipher = CryptoUtil.encrypt(publicKey, Algorithm.SM2, Padding.NONE,null,"zhangzz".getBytes());
        //字节数组转换为ASN.1
        SM2Cipher sm2Cipher = encodeSM2Cipher(cipher, SM2Engine.Mode.C1C3C2);
    }

    /**
     * 密文数组转换为ASN.1
     *
     * @param cipher 默认输入C1C3C2顺序的密文。C1为65字节第1字节为压缩标识，这里固定为0x04，后面64字节为xy分量各32字节。C3为32字节。C2长度与原文一致。
     * @return DER编码后的密文
     * @throws IOException
     */
    public static SM2Cipher encodeSM2Cipher(byte[] cipher, SM2Engine.Mode mode){
        int curveLength = CryptoUtil.CURVE_LEN;
        try {
            return encodeSM2Cipher(mode, curveLength, CryptoUtil.SM3_DIGEST_LENGTH, cipher);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage(),e);
        }
    }

    /**
     * @param mode         指定密文结构，旧标准的为C1C2C3，新的[《SM2密码算法使用规范》 GM/T 0009-2012]标准为C1C3C2
     * @param curveLength  曲线长度，SM2的话就是256位。
     * @param digestLength 摘要长度，如果是SM2的话因为默认使用SM3摘要，SM3摘要长度为32字节。
     * @param cipher       根据mode不同，需要输入的密文C1C2C3排列顺序不同。C1为65字节第1字节为压缩标识，这里固定为0x04，后面64字节为xy分量各32字节。C3为32字节。C2长度与原文一致。
     * @return 按指定mode 的密文
     * @throws Exception
     */
    public static SM2Cipher encodeSM2Cipher(SM2Engine.Mode mode, int curveLength, int digestLength, byte[] cipher)
            throws Exception {

        byte[] c1x = new byte[curveLength];
        byte[] c1y = new byte[curveLength];
        byte[] c2 = new byte[cipher.length - c1x.length - c1y.length - 1 - digestLength];
        byte[] c3 = new byte[digestLength];

        int startPos = 1;
        System.arraycopy(cipher, startPos, c1x, 0, c1x.length);
        startPos += c1x.length;
        System.arraycopy(cipher, startPos, c1y, 0, c1y.length);
        startPos += c1y.length;
        if (mode == SM2Engine.Mode.C1C2C3) {
            System.arraycopy(cipher, startPos, c2, 0, c2.length);
            startPos += c2.length;
            System.arraycopy(cipher, startPos, c3, 0, c3.length);
        } else if (mode == SM2Engine.Mode.C1C3C2) {
            System.arraycopy(cipher, startPos, c3, 0, c3.length);
            startPos += c3.length;
            System.arraycopy(cipher, startPos, c2, 0, c2.length);
        } else {
            throw new Exception("Unsupported mode:" + mode);
        }

        byte[] x = ByteUtil.byteMerger(new byte[32], c1x);
        byte[] y = ByteUtil.byteMerger(new byte[32], c1y);
        byte[] cipherBytes = c2;
        byte[] hashValue = c3;
        int length = (c2.length);
        SM2Cipher sm2Cipher = new SM2Cipher(x,y,hashValue,cipherBytes,length);
        return sm2Cipher;
    }
}
