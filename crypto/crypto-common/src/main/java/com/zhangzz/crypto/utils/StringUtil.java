package com.zhangzz.crypto.utils;

/**
 * 字符串工具类
 */
public class StringUtil {

    /**
     * 判读字符串是否为空
     * @param str
     * @return
     */
    public static boolean isEmpty(String str){

        if(str == null || str.trim().equals("")){
            return true;
        }
        return false;
    }
}
