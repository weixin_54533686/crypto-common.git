package com.zhangzz.crypto.utils;


import com.zhangzz.crypto.core.entity.KeyInfo;
import com.zhangzz.crypto.core.exception.CryptoException;
import com.zhangzz.crypto.enums.Padding;

import java.util.concurrent.Callable;

/**
 * 多线程加密的实现类
 * @author zhangzz
 */
public class Encryptor implements Callable<byte[]> {

    //待加解密的数据
    private byte[] cache;

    //true为加密，false为解密
    private boolean isEncrypt = true;

    //加解密的密钥、算法、初始向量
    private KeyInfo info;

    public byte[] call()throws CryptoException{
        if(isEncrypt) {
            return encrypt();
        }else{
            return decrypt();
        }
    }

    /**
     * 加密
     */

    public byte[] encrypt() throws CryptoException{

        Padding padding = Padding.NOPADDING;
        if(cache.length % 16 >0){
            padding = Padding.PKCS7PADDING;
        }
        return CryptoUtil.encrypt(info.getKey(),info.getAlg() , padding, info.getIv(), cache);
    }


    /**
     * 解密
     */
    public byte[] decrypt()throws CryptoException {

        Padding padding = Padding.NOPADDING;
        if(cache.length % 16 >0){
            padding = Padding.PKCS7PADDING;
        }
          return CryptoUtil.decrypt(info.getKey(),info.getAlg() , padding, info.getIv(), cache);
    }

    public void setCache(byte[] cache){
        this.cache = cache;
    }

    public void setEncrypt(boolean isEncrypt){
        this.isEncrypt = isEncrypt;
    }

    public void setInfo(KeyInfo info) {
        this.info = info;
    }

}
