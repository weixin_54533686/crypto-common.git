package com.zhangzz.crypto.core.entity;


import com.zhangzz.crypto.enums.Algorithm;

/**
 * 加解密的密钥、算法、初始向量的实体类
 */
public class KeyInfo {

    private byte[] key;

    private byte[] iv;

    private Algorithm alg;

    public KeyInfo(byte[] key, Algorithm alg, byte[] iv){
        this.alg = alg;
        this.iv = iv;
        this.key = key;
    }

    public byte[] getKey() {
        return key;
    }

    public void setKey(byte[] key) {
        this.key = key;
    }

    public byte[] getIv() {
        return iv;
    }

    public void setIv(byte[] iv) {
        this.iv = iv;
    }

    public Algorithm getAlg() {
        return alg;
    }

    public void setAlg(Algorithm alg) {
        this.alg = alg;
    }
}
