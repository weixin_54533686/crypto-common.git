package com.zhangzz.crypto.starter.service;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.List;

/**
 * 数字证书组件
 * @author zhangzz
 */
public interface Certificate {

    /**
     * 验证用户证书的有效性，包括验证有效期、证书信任列表、吊销状态等
     *
     * @param certFile       用户证书
     * @param certChainFile  证书链文件
     * @param crlFile        证书吊销列表文件
     * @return true为证书有效，false为无效
     */
    public boolean verifyCertificate(File certFile, File certChainFile, File crlFile);


    /**
     * 验证用户证书的有效性，包括验证有效期、证书信任列表等
     *
     * @param certFile      base64编码的用户证书
     * @param certChainFile      证书链文件
     * @return true为证书有效，false为无效
     */
    public boolean verifyCertificate(File certFile, File certChainFile) ;

    /**
     * 获取证书中的CRL颁发点
     * @param certificate 数字证书
     * @return CRL颁发点的http地址
     */
    public List<String> getCrlByCert(X509Certificate certificate);

    /**
     * 获取证书中的CRL颁发点
     *
     * @param certificate 数字证书
     * @return CRL颁发点的http地址
     */
    public List<String> getCrlByCert(File certificate);
}
