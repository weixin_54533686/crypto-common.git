package com.zhangzz.crypto.core.asn1;

import com.zhangzz.crypto.core.exception.CommonException;
import com.zhangzz.crypto.enums.SignAlgorithm;
import com.zhangzz.crypto.utils.CryptoUtil;
import org.bouncycastle.asn1.*;
import org.bouncycastle.util.encoders.Base64;

import java.io.IOException;
import java.math.BigInteger;

/**
 * sm2签名值
 * @author zhangzz
 *
 */
public class SM2Signature extends ASN1Object{

	private ASN1Integer r;
	
	private ASN1Integer s;

	public SM2Signature(byte[] r, byte[]s) {
		
		this.r = new ASN1Integer(new BigInteger(r));
		this.s = new ASN1Integer(new BigInteger(s));
	}

    public static SM2Signature getInstance(Object obj) {
    	
    	if (obj instanceof SM2Signature)
        {
            return (SM2Signature)obj;
        }

        if (obj != null)
        {
            return new SM2Signature(ASN1Sequence.getInstance(obj));
        }
        throw new CommonException("非空的ASN1对象或DER编码的字节数组");
    }
    
    public SM2Signature(ASN1Sequence seq) {
    	
    	this.r = (ASN1Integer)seq.getObjectAt(0);
    	this.s = (ASN1Integer)seq.getObjectAt(1);
    }
    
    
    public static SM2Signature getInstance(
            ASN1TaggedObject obj,
            boolean explicit)
    {
    	return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }
	
	public BigInteger getR() {
		return this.r.getValue();
	}
	
	public BigInteger getS() {
		return this.s.getValue();
	}
	
	@Override
	public ASN1Primitive toASN1Primitive() {
		ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(r);
        v.add(s);
        return new DERSequence(v);
	}
}
