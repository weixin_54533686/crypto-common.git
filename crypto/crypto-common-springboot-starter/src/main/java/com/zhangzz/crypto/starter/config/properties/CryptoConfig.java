package com.zhangzz.crypto.starter.config.properties;

import com.zhangzz.crypto.enums.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("crypto.config")
@Setter
@Getter
public class CryptoConfig {

    /**
     * 对称密钥，密钥为Base64编码
     */
    private String key = "+NCTEZCw6eW02yMDWtFdcg==";

    /**
     * 非对称密钥对
     */
    private ASymmetricKey aSymmetricKey = new ASymmetricKey();

    /**
     * 对称加解密算法，可选值参考枚举 Algorithm
     */
    private String encryptionAlgorithm = Algorithm.SM4_CBC.getAlg();

    /**
     * 签名算法，可选值参考枚举 Algorithm，算法与公私钥的算法相关
     */
    private String signAlgorithm = SignAlgorithm.SM3WithSM2.getValue();

    /**
     * 摘要算法，可选项参考枚举DigestAlgorithm
     */
    private String digestAlgorithm = DigestAlgorithm.SM3.getValue();

    /**
     * 基于摘要算法的消息认证码算法，可选值参考枚举MacAlgorithm
     */
    private String macAlgorithm = MacAlgorithm.HMACSM3.getValue();

    /**
     * 加解密运算的填充模式，可选值参考枚举 Padding
     */
    private String padding = Padding.PKCS7PADDING.getPadding();

}
