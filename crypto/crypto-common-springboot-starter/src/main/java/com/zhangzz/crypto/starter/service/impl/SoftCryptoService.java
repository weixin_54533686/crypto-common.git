package com.zhangzz.crypto.starter.service.impl;

import com.zhangzz.crypto.core.exception.AlgorithmException;
import com.zhangzz.crypto.core.exception.CryptoException;
import com.zhangzz.crypto.enums.*;
import com.zhangzz.crypto.starter.config.properties.CryptoConfig;
import com.zhangzz.crypto.starter.service.Crypto;
import com.zhangzz.crypto.utils.CryptoUtil;
import org.bouncycastle.util.encoders.Base64;

import java.io.File;
import java.security.cert.X509Certificate;

/**
 * 简化的加解密实现
 */
public class SoftCryptoService implements Crypto {

    private CryptoConfig config;

    private final ThreadLocal<CryptoConfig> local = new ThreadLocal<>();

    public SoftCryptoService(CryptoConfig config){
        this.config = config;
    }


    @Override
    public String encrypt(String data) throws CryptoException {
        byte[] result = CryptoUtil.encrypt(this.getEncryptKey(),this.getAlgorithm(),this.gePadding(),null,data.getBytes());
        return Base64.toBase64String(result);
    }


    @Override
    public String decrypt(String data) throws CryptoException {

        byte[] result = CryptoUtil.decrypt(this.getDecryptKey(),this.getAlgorithm(),this.gePadding(),null,data.getBytes());

        return new String(result);
    }

    @Override
    public String sign(String data)throws CryptoException{

       byte[] signValue =  CryptoUtil.sign(this.getPrivateKey(),getSignAlgorithm(),data.getBytes());
       return Base64.toBase64String(signValue);
    }

    @Override
    public boolean verificationSignature(String signValue,String data)throws CryptoException{
        return CryptoUtil.verificationSignature(this.getPublicKey(),this.getSignAlgorithm(),Base64.decode(signValue),data.getBytes());
    }

    @Override
    public boolean verificationSignature(X509Certificate certificate, String signValue, String data)throws CryptoException{
        return CryptoUtil.verificationSignature(certificate,this.getSignAlgorithm(),Base64.decode(signValue),data.getBytes());
    }

    @Override
    public String getHash(String data)throws CryptoException{
        byte[] hashValue = CryptoUtil.getHash(data.getBytes(),getDigestAlgorithm());
        return Base64.toBase64String(hashValue);
    }

    @Override
    public String mac(String inData)throws CryptoException{
        byte[] macValue = CryptoUtil.mac(this.getKey(),this.getMacAlgorithm(),null,inData.getBytes());
        return Base64.toBase64String(macValue);
    }

    @Override
    public void encryptFile(String filePath, String outFilePath)throws CryptoException{
        CryptoUtil.encryptFile(filePath,outFilePath,this.getKey(),this.getAlgorithm(),null);
    }

    @Override
    public void encryptFile(File file, File outFile)throws CryptoException{
        CryptoUtil.encryptFile(file,outFile,this.getKey(),this.getAlgorithm(),null);
    }

    public void decryptFile(String filePath, String outFilePath)throws CryptoException{
        CryptoUtil.decryptFile(filePath,outFilePath,this.getKey(),this.getAlgorithm(),null);
    }

    public void decryptFile(File file, File outFile){
        CryptoUtil.decryptFile(file,outFile,this.getKey(),this.getAlgorithm(),null);
    }

    /**
     * 添加临时配置，仅当前线程有效。
     *
     * @param config
     */
    @Override
    public void setTempConfig(CryptoConfig config) {
        local.set(config);
    }


    private  byte[] getEncryptKey(){

        String alg = null;
        if(local.get() == null){
            alg = this.config.getEncryptionAlgorithm();
            if(alg.startsWith("RSA")||alg.startsWith("SM2")){
                String key =  this.config.getASymmetricKey().getPublicKey();
                return Base64.decode(key);
            }else{
                return Base64.decode(this.config.getKey());
            }
        }else{

            if(local.get().getEncryptionAlgorithm() != null){
                alg = local.get().getEncryptionAlgorithm();
            }else{
                alg = this.config.getEncryptionAlgorithm();
            }

            if(alg.startsWith("RSA")||alg.startsWith("SM2")){
                String key = null;
                if(local.get().getASymmetricKey() != null && local.get().getASymmetricKey().getPublicKey() != null) {
                    key = local.get().getASymmetricKey().getPublicKey();
                }else{
                    key = this.config.getASymmetricKey().getPublicKey();
                }
                return Base64.decode(key);
            }else{
                if(local.get().getKey()!=null){
                    return Base64.decode(this.local.get().getKey());
                }else{
                    return Base64.decode(this.config.getKey());
                }
            }
        }


    }

    private  byte[] getKey(){

        String alg = null;
        if(local.get() == null || local.get().getKey() == null){
            return Base64.decode(this.config.getKey());
        }else{
            return Base64.decode(this.local.get().getKey());
        }


    }

    private  byte[] getDecryptKey(){

        String alg = null;
        if(local.get() == null){
            alg = this.config.getEncryptionAlgorithm();
            if(alg.startsWith("RSA")||alg.startsWith("SM2")){
                String key =  this.config.getASymmetricKey().getPrivateKey();
                return Base64.decode(key);
            }else{
                return Base64.decode(this.config.getKey());
            }
        }else{

            if(local.get().getEncryptionAlgorithm() != null){
                alg = local.get().getEncryptionAlgorithm();
            }else{
                alg = this.config.getEncryptionAlgorithm();
            }

            if(alg.startsWith("RSA")||alg.startsWith("SM2")){
                String key = null;
                if(local.get().getASymmetricKey() != null && local.get().getASymmetricKey().getPrivateKey() != null) {
                    key = local.get().getASymmetricKey().getPrivateKey();
                }else{
                    key = this.config.getASymmetricKey().getPrivateKey();
                }
                return Base64.decode(key);
            }else{
                if(local.get().getKey()!=null){
                    return Base64.decode(this.local.get().getKey());
                }else{
                    return Base64.decode(this.config.getKey());
                }
            }
        }
    }

    private Algorithm getAlgorithm(){


        if(local.get() == null || local.get().getEncryptionAlgorithm() == null){
            return this.getAlg(this.config.getEncryptionAlgorithm());
        }else{
            return this.getAlg(local.get().getEncryptionAlgorithm());
        }
    }

    public Algorithm getAlg(String alg){

        for(Algorithm algorithm:Algorithm.values()){
            if(algorithm.getAlg().equalsIgnoreCase(alg)){
                return algorithm;
            }
        }

        throw new AlgorithmException("未知的算法！");
    }

    private SignAlgorithm getSignAlgorithm(){

        if(local.get() == null || local.get().getSignAlgorithm() == null){
            return this.getSignAlg(this.config.getSignAlgorithm());
        }else{
            return this.getSignAlg(local.get().getSignAlgorithm());
        }
    }

    public SignAlgorithm getSignAlg(String alg){

        for(SignAlgorithm algorithm:SignAlgorithm.values()){
            if(algorithm.getValue().equalsIgnoreCase(alg)){
                return algorithm;
            }
        }

        throw new AlgorithmException("未知的算法！");
    }

    private DigestAlgorithm getDigestAlgorithm(){

        if(local.get() == null || local.get().getDigestAlgorithm() == null){
            return this.getDigestAlg(this.config.getDigestAlgorithm());
        }else{
            return this.getDigestAlg(local.get().getDigestAlgorithm());
        }
    }

    public DigestAlgorithm getDigestAlg(String alg){

        for(DigestAlgorithm algorithm:DigestAlgorithm.values()){
            if(algorithm.getValue().equalsIgnoreCase(alg)){
                return algorithm;
            }
        }

        throw new AlgorithmException("未知的算法！");
    }

    private MacAlgorithm getMacAlgorithm(){

        if(local.get() == null || local.get().getMacAlgorithm() == null){
            return this.getMacAlg(this.config.getMacAlgorithm());
        }else{
            return this.getMacAlg(local.get().getMacAlgorithm());
        }
    }

    private MacAlgorithm getMacAlg(String alg){

        System.out.println("---------->:"+alg);
        for(MacAlgorithm algorithm:MacAlgorithm.values()){
            if(algorithm.getValue().equalsIgnoreCase(alg)){
                return algorithm;
            }
        }

        throw new AlgorithmException("未知的算法！");
    }

    private Padding gePadding(){

        String alg = null;
        if(local.get() == null || local.get().getEncryptionAlgorithm() == null){
            alg = this.config.getEncryptionAlgorithm();
        }else{
            alg = this.local.get().getEncryptionAlgorithm();
        }

        if(alg.startsWith("RSA")){
            return Padding.PKCS1Padding;
        }else if(alg.startsWith("SM2")){
            return Padding.NONE;
        }else{
            return Padding.PKCS7PADDING;
        }
    }

    private byte[] getPrivateKey(){
        String privateKey = null;
        if(local.get() == null || local.get().getASymmetricKey().getPrivateKey() == null){
            privateKey =  this.config.getASymmetricKey().getPrivateKey();
        }else{
            privateKey = this.local.get().getASymmetricKey().getPrivateKey();
        }
        return Base64.decode(privateKey);
    }

    private byte[] getPublicKey(){
        String publicKey = null;
        if(local.get() == null || local.get().getASymmetricKey().getPublicKey() == null){
            publicKey =  this.config.getASymmetricKey().getPublicKey();
        }else{
            publicKey = this.local.get().getASymmetricKey().getPublicKey();
        }
        return Base64.decode(publicKey);
    }
}
