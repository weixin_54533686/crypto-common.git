package com.zhangzz.crypto.utils;

import com.zhangzz.crypto.core.entity.KeyInfo;
import com.zhangzz.crypto.core.entity.ZKeyPair;
import com.zhangzz.crypto.core.exception.CryptoException;
import com.zhangzz.crypto.core.thread.ThreadHelper;
import com.zhangzz.crypto.enums.*;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.math.ec.custom.gm.SM2P256V1Curve;
import org.bouncycastle.util.encoders.Base64;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.X509Certificate;
import java.security.spec.ECFieldFp;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.EllipticCurve;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author zhangzz
 */
public class CryptoUtil {

    static {
        if(Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    /*
     * 以下为SM2推荐曲线参数
     */
    public static final SM2P256V1Curve CURVE = new SM2P256V1Curve();
    public final static BigInteger SM2_ECC_P = CURVE.getQ();
    public final static BigInteger SM2_ECC_A = CURVE.getA().toBigInteger();
    public final static BigInteger SM2_ECC_B = CURVE.getB().toBigInteger();
    public final static BigInteger SM2_ECC_N = CURVE.getOrder();
    public final static BigInteger SM2_ECC_H = CURVE.getCofactor();
    public final static BigInteger SM2_ECC_GX = new BigInteger(
            "32C4AE2C1F1981195F9904466A39C9948FE30BBFF2660BE1715A4589334C74C7", 16);
    public final static BigInteger SM2_ECC_GY = new BigInteger(
            "BC3736A2F4F6779C59BDCEE36B692153D0A9877CC62A474002DF32E52139F0A0", 16);
    public static final ECPoint G_POINT = CURVE.createPoint(SM2_ECC_GX, SM2_ECC_GY);
    public static final ECDomainParameters DOMAIN_PARAMS = new ECDomainParameters(CURVE, G_POINT,
            SM2_ECC_N, SM2_ECC_H);

    public static final int CURVE_LEN =  (DOMAIN_PARAMS.getCurve().getFieldSize() + 7) / 8;

    public static final EllipticCurve JDK_CURVE = new EllipticCurve(new ECFieldFp(SM2_ECC_P), SM2_ECC_A, SM2_ECC_B);

    public static final java.security.spec.ECPoint JDK_G_POINT = new java.security.spec.ECPoint(
            G_POINT.getAffineXCoord().toBigInteger(), G_POINT.getAffineYCoord().toBigInteger());

    public static final java.security.spec.ECParameterSpec JDK_EC_SPEC = new java.security.spec.ECParameterSpec(
            JDK_CURVE, JDK_G_POINT, SM2_ECC_N, SM2_ECC_H.intValue());
    public static final int SM3_DIGEST_LENGTH = 32;

    /**
     * 加密方法
     * @param key 密钥（可为对称密钥或非对称密钥对的公钥）
     * @param alg 加密算法
     * @param pad 填充模式
     * @param iv iv初始向量
     * @param data 待加密的数据
     * @return 返回加密后的密文
     */
    public static byte[] encrypt(byte[] key, Algorithm alg, Padding pad,byte[] iv,byte[] data)throws CryptoException{

        String algorithm = alg.getAlg();
        if((algorithm.contains("CBC") || algorithm.contains("OFB") || algorithm.contains("CFB")) && (iv == null || iv.length == 0)) {
            iv = "0000000000000001".getBytes();
        }

        Key keyObj = null;
        String[] strs = algorithm.split("/");

        if(strs[0].equals("RSA") || strs[0].equals("SM2")) {

            keyObj = SoftwareKeyUtil.bytesToPublicKey(key, strs[0]);
        }else{

            keyObj = SoftwareKeyUtil.bytesToSecretkey(key,strs[0]);
        }

        return cipher(keyObj,algorithm,pad,iv,data,Cipher.ENCRYPT_MODE);
    }


    /**
     * 解密方法
     * @param key 密钥（可为对称密钥或非对称密钥对的私钥）
     * @param alg 解密算法
     * @param pad 填充模式
     * @param iv iv初始向量
     * @param data 待解密的数据
     * @return 返回解密后的明文
     */
    public static byte[] decrypt(byte[] key, Algorithm alg, Padding pad,byte[] iv,byte[] data)throws CryptoException{

        String algorithm = alg.getAlg();
        if((algorithm.contains("CBC") || algorithm.contains("OFB") || algorithm.contains("CFB")) && (iv == null || iv.length == 0)) {
            iv = "0000000000000001".getBytes();
        }

        Key keyObj = null;
        String[] strs = algorithm.split("/");

        if(strs[0].equals("RSA") || strs[0].equals("SM2")) {

            keyObj = SoftwareKeyUtil.bytesToPrivateKey(key, strs[0]);
        }else{

            keyObj = SoftwareKeyUtil.bytesToSecretkey(key,strs[0]);
        }

        return cipher(keyObj,algorithm,pad,iv,data,Cipher.DECRYPT_MODE);
    }

    private static byte[] cipher(Key key, String algorithm, Padding pad,byte[] iv,byte[] data,int cipherModel)throws CryptoException{

        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance(algorithm+"/"+pad.getPadding(),BouncyCastleProvider.PROVIDER_NAME);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new CryptoException("未找到算法",e);
        }  catch (NoSuchPaddingException e) {
            e.printStackTrace();
            throw new CryptoException("未找到填充模式",e);
        }catch (NoSuchProviderException e){
            e.printStackTrace();
            throw new CryptoException("当需要特定的安全提供程序但在环境中不可用时，会引发此异常："+e.getMessage(),e);
        }

        try {
            if(iv != null && iv.length > 0){
                cipher.init(cipherModel, key,new IvParameterSpec(iv));
            }else {
                cipher.init(cipherModel, key);
            }

        } catch (InvalidKeyException e) {
            e.printStackTrace();
            throw new CryptoException("无效密钥",e);
        }catch(InvalidAlgorithmParameterException e){
            e.printStackTrace();
            throw new CryptoException("无效iv初始向量",e);
        }

        byte[] cipherData = null;
        try {
            cipherData = cipher.doFinal(data);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
            throw new CryptoException("非法块大小异常",e);
        } catch (BadPaddingException e) {
            e.printStackTrace();
            throw new CryptoException("错误的填充",e);
        }
        return cipherData;
    }


    /**
     * 数字签名
     * @param privateKey 私钥（PKCS8编码的私钥字节数组，可参考非对称密钥对的生成接口返回结果。）
     * @param alg 签名算法
     * @param data 待签名的数据
     * @return
     */
    public static byte[] sign(byte[] privateKey, SignAlgorithm alg, byte[] data)throws CryptoException{


        PrivateKey key = null;
        String signAlg = alg.getValue();

        if(signAlg.endsWith("RSA")) {

            key = SoftwareKeyUtil.bytesToPrivateKey(privateKey, "RSA");
        }else{
            key = SoftwareKeyUtil.bytesToPrivateKey(privateKey, "SM2");
        }

        Signature inSignatue = null;
        try {
            inSignatue = Signature.getInstance(signAlg, BouncyCastleProvider.PROVIDER_NAME);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new CryptoException("未找到算法",e);
        }catch (NoSuchProviderException e){
            throw new CryptoException("未找到安全程序提供商，原因:"+e.getMessage(),e);
        }


        try {
            inSignatue.initSign(key);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            throw new CryptoException("无效的密钥",e);
        }
        try {
            inSignatue.update(data);
        } catch (SignatureException e) {
            e.printStackTrace();
            throw new CryptoException("签名更新数据异常",e);
        }
        byte[] signOut = null;
        try {
            signOut = inSignatue.sign();
        } catch (SignatureException e) {
            e.printStackTrace();
            throw new CryptoException("签名异常",e);
        }
        return signOut;
    }


    /**
     * 数字签名验签
     * @param publicKey 公钥（PKCS8编码的私钥字节数组，可参考非对称密钥对的生成接口返回结果。）
     * @param alg 签名算法
     * @param signValue 签名值
     * @param data 签名值的原文
     * @return true为验签通过，表示签名者是合法的。false为验证未通过，签名者为非法。
     */
    public static boolean verificationSignature(byte[] publicKey, SignAlgorithm alg, byte[] signValue,byte[] data)throws CryptoException{

        PublicKey key = null;
        String signAlg = alg.getValue();

        if(signAlg.endsWith("RSA")) {

            key = SoftwareKeyUtil.bytesToPublicKey(publicKey, "RSA");
        }else{
            key = SoftwareKeyUtil.bytesToPublicKey(publicKey, "SM2");
        }

        Signature inSignatue = null;
        try {
            inSignatue = Signature.getInstance(signAlg, BouncyCastleProvider.PROVIDER_NAME);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new CryptoException("未找到算法",e);
        }catch (NoSuchProviderException e){
            throw new CryptoException("未找到安全程序提供商，原因:"+e.getMessage(),e);
        }


        try {
            inSignatue.initVerify(key);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            throw new CryptoException("无效的公钥",e);
        }
        try {
            inSignatue.update(data);
        } catch (SignatureException e) {
            e.printStackTrace();
            throw new CryptoException("签名更新数据异常",e);
        }
        try {
            return inSignatue.verify(signValue);
        } catch (SignatureException e) {
            e.printStackTrace();
            throw new CryptoException("签名异常",e);
        }
    }

    /**
     * 数字签名验签
     * @param certificate 数字证书
     * @param alg 签名算法
     * @param signValue 签名值
     * @param data 签名值的原文
     * @return true为验签通过，表示签名者是合法的。false为验证未通过，签名者为非法。
     */
    public static boolean verificationSignature(X509Certificate certificate, SignAlgorithm alg, byte[] signValue, byte[] data)throws CryptoException{

        PublicKey publicKey = certificate.getPublicKey();
        return verificationSignature(publicKey.getEncoded(),alg,signValue,data);
    }

    /**
     * 对数据做摘要（哈希/散列）算法运算，得到摘要值
     * @param data 原数据
     * @param digestAlgorithm 摘要算法
     * @return 摘要值
     */
    public static byte[] getHash(byte[] data, DigestAlgorithm digestAlgorithm)throws CryptoException{
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance(digestAlgorithm.getValue(),BouncyCastleProvider.PROVIDER_NAME);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new CryptoException("未找到算法",e);
        }catch (NoSuchProviderException e){
            throw new CryptoException("当需要特定的安全提供程序但在环境中不可用时，会引发此异常："+e.getMessage(),e);
        }

        messageDigest.update(data);
        byte[] output = messageDigest.digest();
        return output;
    }

    /**
     * 消息鉴别码运算
     * @param key       密钥
     * @param algorithm 算法
     * @param iv iv初始向量，若算法为CMAC，则iv必填，否则可为null。
     * @param inData    待运算数据
     * @return mac值
     */
    public static byte[] mac(byte[] key, MacAlgorithm algorithm, byte[] iv, byte[] inData)throws CryptoException {

        if(algorithm.getValue().endsWith("CMAC") && (iv == null || iv.length == 0)) {
            iv = "0000000000000001".getBytes();
        }

        SecretKeySpec keySpec = new SecretKeySpec(key,algorithm.getValue());
        Mac mac = null;
        try {
            mac = Mac.getInstance(algorithm.getValue(),BouncyCastleProvider.PROVIDER_NAME);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new CryptoException("未找到算法",e);
        }catch (NoSuchProviderException e){
            e.printStackTrace();
            throw new CryptoException("当需要特定的安全提供程序但在环境中不可用时，会引发此异常："+e.getMessage(),e);
        }

        try {
            if(iv != null && iv.length > 0){
                mac.init(keySpec,new IvParameterSpec(iv));
            }else {
                mac.init(keySpec);
            }

        } catch (InvalidKeyException e) {
            e.printStackTrace();
            throw new CryptoException("无效密钥",e);
        }catch(InvalidAlgorithmParameterException e) {
            e.printStackTrace();
            throw new CryptoException("无效IV",e);
        }

        mac.update(inData);
        return mac.doFinal();
    }

    /**
     * 生成对称密钥
     * @param algorithm 算法（推荐算法使用：SM4、AES、DESEDE）
     * @param keyLen    密钥模长
     * @return 对称密钥
     */
    public static byte[] generateKey(KeyAlgorithmId algorithm, int keyLen)throws CryptoException {

        KeyGenerator kg = null;
        try {
            kg = KeyGenerator.getInstance(algorithm.getValue(), BouncyCastleProvider.PROVIDER_NAME);
        }  catch (NoSuchAlgorithmException e) {
            throw new CryptoException("未知的算法："+e.getMessage(), e);
        } catch (NoSuchProviderException e) {
            throw new CryptoException("当需要特定的安全提供程序但在环境中不可用时，会引发此异常："+e.getMessage(),e);
        }
        kg.init(keyLen);
        return kg.generateKey().getEncoded();
    }

    /**
     * 生成随机数
     *
     * @param size 随机数长度
     * @return 随机数
     */
    public static byte[] generateRandom(int size)throws CryptoException {

        SecureRandom secureRandom = null;
        try {
            secureRandom = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new CryptoException("未找到算法",e);
        }

        /*设置种子为可选步骤，可以直接生成随机数*/
        byte[] seed = secureRandom.generateSeed(12);
        secureRandom.setSeed(seed);
        byte[] random = new byte[size];
        secureRandom.nextBytes(random);
        return random;
    }

    /**
     * 生成非对称密钥对
     * @param algorithmId 非对称密钥对算法ID
     * @param length 密钥模长
     * @return
     */
    public static ZKeyPair generateKeyPair(KeyPairAlgorithmId algorithmId,int length)throws CryptoException{
        String alg = algorithmId.getAlg();
        if(algorithmId.getAlg().equals("SM2")){
            alg = "EC";
        }
        // 获取一个椭圆曲线类型的密钥对生成器
        KeyPairGenerator kpg = null;
        try {
            kpg = KeyPairGenerator.getInstance(alg, BouncyCastleProvider.PROVIDER_NAME);
        } catch (NoSuchAlgorithmException e) {
            throw new CryptoException("未找到算法："+e.getMessage(), e);
        }catch (NoSuchProviderException e){
            throw new CryptoException("未找到密钥提供商："+e.getMessage(), e);
        }

        if(algorithmId.getAlg().equals("SM2")) {
            // 使用SM2的算法区域初始化密钥生成器
            try {
                kpg.initialize(new ECGenParameterSpec("sm2p256v1"), new SecureRandom());
            } catch (InvalidAlgorithmParameterException e) {
                throw new CryptoException("这是无效或不适当算法参数的异常情况：" + e.getMessage(), e);
            }
        }else{
            kpg.initialize(length);
        }
        // 获取密钥对
        KeyPair keyPair = kpg.generateKeyPair();

        ZKeyPair eKeyPair = new ZKeyPair();
        eKeyPair.setPrivateKey(keyPair.getPrivate().getEncoded());
        eKeyPair.setPublicKey(keyPair.getPublic().getEncoded());
        return eKeyPair;
    }

    /**
     * 将字节数组进行Base64编码
     * @param value 待编码的字节数组
     * @return Base64编码的字符串
     */
    public static String toBase64(byte[] value){
        return Base64.toBase64String(value);
    }

    /**
     * 将Base64编码的字符串进行解码
     * @param base64Value 待解码的字符串
     * @return byte数组
     */
    public static byte[] base64Decode(String base64Value){
        return Base64.decode(base64Value);
    }


    //每次分割文件的大小，单位M(默认1M，避免内存溢出)
    private final static int spitSize = (1024*1024);


    /**
     * 文件解密接口
     * @param filePath 待加密的文件路径
     * @param outFilePath 加密后的密文文件路径
     * @param key 对称密钥的key
     * @param alg 加密算法
     * @param iv 初始向量
     */
    public static void encryptFile(String filePath, String outFilePath, byte[] key, Algorithm alg, byte[] iv)throws CryptoException{

        File file = new File(filePath);

        File outFile = new File(outFilePath);
        //创建输出文件
        if(!outFile.exists()){
            if(!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }

            try {
                outFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                throw new CryptoException("文件加密的，输出文件创建失败，请尝试手动创建！",e);
            }
        }

        encryptFile(file,outFile,key,alg,iv);
    }

    /**
     * 文件解密接口
     * @param file 待加密的文件
     * @param outFile 密文文件
     * @param key 对称密钥的key
     * @param alg 加密算法
     * @param iv 初始向量
     */
    public static void encryptFile(File file, File outFile, byte[] key, Algorithm alg, byte[] iv)throws CryptoException{

        InputStream fileInput = null;
        try {
            fileInput = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            throw new CryptoException("文件加密的，输出文件创建失败，请尝试手动创建！",e);
        }

        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(outFile);
        }catch (FileNotFoundException e){
            //输出文件未找到，则关闭输入流
            try {
                if (fileInput != null) {
                    fileInput.close();
                }
            } catch (IOException e1) {

                e.printStackTrace();
            }
        }

        byte[] cache = new byte[spitSize];
        //获取线程池
        ThreadPoolExecutor executor = ThreadHelper.newInstance().getThreadPoolExecutor();
        //线程执行结果集合
        List<Future<byte[]>> futureList = new ArrayList<Future<byte[]>>();
        Encryptor enc = null;
        int byteSize = 0;
        try {
            byte[] cipherBytes = null;
            while((byteSize = fileInput.read(cache))!=-1){

                enc = new Encryptor();
                if(byteSize != spitSize){
                    byte[] lastPartBuffer = new byte[byteSize];
                    System.arraycopy(cache, 0, lastPartBuffer, 0, byteSize);
                    enc.setCache(lastPartBuffer);
                }else {
                    cipherBytes = new byte[spitSize];
                    System.arraycopy(cache, 0, cipherBytes, 0, spitSize);
                    enc.setCache(cipherBytes);
                }
                enc.setEncrypt(true);
                enc.setInfo(new KeyInfo(key,alg,iv));
                futureList.add(executor.submit(enc));
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new CryptoException("待加密文件读取失败，原因："+e.getMessage(),e);
        }finally {
            //文件读取完毕，关闭输入流
            try {
                if(fileInput != null){
                    fileInput.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        writeFile(outputStream,futureList);

    }

    /**
     * 文件解密接口
     * @param filePath 待解密的文件路径
     * @param outFilePath 解密后的明文文件路径
     * @param key 对称密钥的key
     * @param alg 解密算法
     * @param iv 初始向量
     */
    public static void decryptFile(String filePath, String outFilePath, byte[] key, Algorithm alg, byte[] iv)throws CryptoException{

        File file = new File(filePath);

        File outFile = new File(outFilePath);
        //创建输出文件
        if(!outFile.exists()){
            if(!outFile.getParentFile().exists()){
                outFile.getParentFile().mkdirs();
            }

            try {
                outFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                throw new CryptoException("文件解密，输出文件创建失败，请尝试手动创建！",e);
            }
        }

        decryptFile(file,outFile,key,alg,iv);
    }

    /**
     * 文件解密接口
     * @param file 待解密的文件
     * @param outFile 解密后的明文文件
     * @param key 对称密钥的key
     * @param alg 解密算法
     * @param iv 初始向量
     */
    public static void decryptFile(File file, File outFile, byte[] key, Algorithm alg, byte[] iv)throws CryptoException{

        InputStream fileInput = null;
        try {
            fileInput = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            throw new CryptoException("文件解密，待解密文件未找到！",e);
        }

        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(outFile);
        }catch (FileNotFoundException e){
            //输出文件未找到，则关闭输入流
            try {
                if (fileInput != null) {
                    fileInput.close();
                }
            } catch (IOException e1) {

                e.printStackTrace();
            }
            throw new CryptoException("解密文件的待输出明文文件未找到！",e);
        }

        byte[] cache = new byte[spitSize];

        ThreadPoolExecutor executor = ThreadHelper.newInstance().getThreadPoolExecutor();
        KeyInfo info = new KeyInfo(key,alg,iv);
        List<Future<byte[]>> futures = new ArrayList<>();
        Encryptor enc = null;
        byte[] plainBytes = null;
        int byteSize = 0;
        try {
            while((byteSize = fileInput.read(cache))!=-1){
                enc = new Encryptor();
                if(byteSize != spitSize){
                    byte[] lastPartBuffer = new byte[byteSize];
                    System.arraycopy(cache, 0, lastPartBuffer, 0, byteSize);
                    enc.setCache(lastPartBuffer);
                }else {
                    plainBytes = new byte[spitSize];
                    System.arraycopy(cache, 0, plainBytes, 0, spitSize);
                    enc.setCache(plainBytes);
                }

                enc.setEncrypt(false);
                enc.setInfo(info);
                futures.add(executor.submit(enc));
            }

        } catch (IOException e) {
            throw new CryptoException("待解密文件读取失败，原因："+e.getMessage(),e);
        }finally {
            try {
                if (fileInput != null) {
                    fileInput.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        writeFile(outputStream, futures);

    }

    /**
     * 将处理完的字节写入输出文件中
     * @param outputStream 输出流
     * @param futures 线程异步返回结果
     */
    private static void writeFile(OutputStream outputStream, List<Future<byte[]>> futures)throws CryptoException{
        try {
            for(Future<byte[]> future: futures){
                outputStream.write(future.get());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new CryptoException("获取线程执行结果失败，原因："+e.getMessage(),e);
        } catch (ExecutionException e) {
            throw new CryptoException("获取线程执行结果失败，原因："+e.getMessage(),e);
        }catch (IOException e){
            e.printStackTrace();
            throw new CryptoException("输入流写入字节数组失败，原因："+e.getMessage(),e);
        }finally {
            //关闭输出流
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
