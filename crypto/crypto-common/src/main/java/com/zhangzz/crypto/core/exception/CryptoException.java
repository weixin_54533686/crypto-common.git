package com.zhangzz.crypto.core.exception;

public class CryptoException extends RuntimeException{

    private static final long serialVersionUID = 2L;

    public CryptoException(String msg, Throwable e) {
        super(msg, e);
    };

    public CryptoException(String msg) {
        super(msg);
    };
}
