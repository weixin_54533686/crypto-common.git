package com.test;

import com.zhangzz.crypto.core.entity.ZKeyPair;
import com.zhangzz.crypto.core.exception.CryptoException;
import com.zhangzz.crypto.enums.KeyAlgorithmId;
import com.zhangzz.crypto.enums.KeyPairAlgorithmId;
import com.zhangzz.crypto.utils.CryptoUtil;
import org.bouncycastle.util.encoders.Base64;

public class Test {

    public static void main(String[] args) {
        try {
            byte[] aa =CryptoUtil.generateKey(KeyAlgorithmId.SM4,128);
            System.out.println(Base64.toBase64String(aa));

            ZKeyPair keyPair = CryptoUtil.generateKeyPair(KeyPairAlgorithmId.SM2,256);
            System.out.println(Base64.toBase64String(keyPair.getPublicKey()));
            System.out.println(Base64.toBase64String(keyPair.getPrivateKey()));

        } catch (CryptoException e) {
            e.printStackTrace();
        }
    }
}
