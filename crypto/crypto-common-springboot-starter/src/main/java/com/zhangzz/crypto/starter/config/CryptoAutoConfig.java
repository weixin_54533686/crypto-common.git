package com.zhangzz.crypto.starter.config;

import com.zhangzz.crypto.starter.config.properties.CryptoConfig;
import com.zhangzz.crypto.starter.service.Certificate;
import com.zhangzz.crypto.starter.service.Crypto;
import com.zhangzz.crypto.starter.service.impl.CertificateService;
import com.zhangzz.crypto.starter.service.impl.SoftCryptoService;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(CryptoConfig.class)
public class CryptoAutoConfig {

    @Bean
    public Crypto getCryptoService(CryptoConfig config){

        return new SoftCryptoService(config);
    }

    @Bean
    public Certificate getCertService(){

        return new CertificateService();
    }
}
