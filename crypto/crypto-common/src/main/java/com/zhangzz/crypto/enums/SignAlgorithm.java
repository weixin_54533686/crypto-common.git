package com.zhangzz.crypto.enums;

/**
 * 签名算法标识
 */
public enum SignAlgorithm {

    SHA256WithRSA("SHA256WithRSA"),
    SHA384WithRSA("SHA384WithRSA"),
    NONEwithRSA("NONEwithRSA"),
    SHA256WithSM2("SHA256WithSM2"),
    SM3WithSM2("SM3WithSM2");

    private String value;

    public String getValue(){
        return this.value;
    }

    SignAlgorithm(String value){
        this.value = value;
    }
}
