package com.zhangzz.crypto.enums;


/**
 * 加解密算法的枚举，此处仅选用目前为止较为安全的算法及加密模式
 * @author zhangzz
 */
public enum Algorithm {

    DEFAULT(null),
    SM2("SM2"),
    RSA_ECB("RSA/ECB"),
    DESEDE_CBC("DESede/CBC"),
    DESEDE_CFB("DESede/CFB"),
    DESEDE_OFB("DESede/OFB"),
    AES_CBC("AES/CBC"),
    AES_CFB("AES/CFB"),
    AES_OFB("AES/OFB"),
    SM4_CBC("SM4/CBC"),
    SM4_CFB("SM4/CFB"),
    SM4_OFB("SM4/OFB"),
    ;

    private String alg;

    public String getAlg() {
        return alg;
    }

    Algorithm(String alg){
        this.alg = alg;
    }

}
