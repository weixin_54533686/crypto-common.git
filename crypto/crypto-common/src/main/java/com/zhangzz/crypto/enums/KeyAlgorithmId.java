package com.zhangzz.crypto.enums;

public enum KeyAlgorithmId {
    DESede("DESede"),SM4("SM4"),AES("AES");

    private String value;

    public String getValue(){
        return this.value;
    }

    KeyAlgorithmId(String value){
        this.value = value;
    }
}
