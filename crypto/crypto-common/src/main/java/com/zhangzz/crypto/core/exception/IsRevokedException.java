package com.zhangzz.crypto.core.exception;

import java.security.cert.CRLReason;

public class IsRevokedException extends Exception{

    private static final long serialVersionUID = 3L;

    /**
     * 吊销理由
     */
    private CRLReason reason;

    public IsRevokedException(CRLReason reason,String msg, Throwable e) {
        super(msg, e);
        this.reason = reason;
    };

    public IsRevokedException(CRLReason reason,String msg) {
        super(msg);
        this.reason = reason;
    }

    public IsRevokedException(CRLReason reason) {
        this.reason = reason;
    }

    /**
     * 获取吊销理由
     * @return 吊销理由
     */
    public CRLReason getReason() {
        return reason;
    };
}
