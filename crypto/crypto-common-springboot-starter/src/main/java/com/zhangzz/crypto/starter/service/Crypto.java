package com.zhangzz.crypto.starter.service;

import com.zhangzz.crypto.starter.config.properties.CryptoConfig;

import java.io.File;
import java.security.cert.X509Certificate;

/**
 * 密码应用接口
 * @author zhangzz
 * @date 2024-04-04
 */
public interface Crypto {

    /**
     * 字符串加密
     * @param data 待加密的字符串
     * @return Base64编码后的字符串密文
     */
    public String encrypt(String data);

    /**
     * 字符串解密
     * @param data 待解密的字符串密文
     * @return 返回字符串明文
     */
    public String decrypt(String data) ;

    /**
     * 对字符串做签名
     * @param data 待签名的字符串
     * @return 返回base64编码后的签名值
     */
    public String sign(String data);

    /**
     * 使用签名值和签名原文做验签，用于验证该签名值是否为该签名原文的签名值（可用于抗抵赖、完整性校验）
     * @param signValue 签名值
     * @param data 签名原文
     * @return
     */
    public boolean verificationSignature(String signValue,String data);

    /**
     * 使用签名值和签名原文做验签，用于验证该签名值是否为该签名原文的签名值（可用于抗抵赖、完整性校验）
     * @param certificate 数字证书（使用数字证书的公钥进行签名验证）
     * @param signValue 签名值
     * @param data 签名原文
     * @return
     */
    public boolean verificationSignature(X509Certificate certificate, String signValue, String data);

    /**
     * 获取数据的摘要值，默认摘要算法为：SM3
     * @param data
     * @return
     */
    public String getHash(String data);

    /**
     * mac运算，默认算法为HMACSM3，若为基于对称算法的mac运算，则使用与对称加解密的密钥
     * @param inData 数据原文
     * @return mac值
     */
    public String mac(String inData);

    /**
     * 文件加密，加密算法默认为：SM4
     * @param filePath 待加密文件路径
     * @param outFilePath 加密后的文件存储路径
     */
    public void encryptFile(String filePath, String outFilePath);

    /**
     * 文件加密，加密算法默认为：SM4
     * @param file 待加密文件
     * @param outFile 加密后的文件
     */
    public void encryptFile(File file, File outFile);

    /**
     * 文件解密
     * @param filePath 待解密文件路径
     * @param outFilePath 解密后的文件路径
     */
    public void decryptFile(String filePath, String outFilePath);

    /**
     * 文件解密
     * @param file 待解密的文件
     * @param outFile 解密后的文件
     */
    public void decryptFile(File file, File outFile);
    /**
     * 添加临时密钥及算法配置，仅当前线程有效。
     * @param config 配置信息
     */
    public void setTempConfig(CryptoConfig config);

}
