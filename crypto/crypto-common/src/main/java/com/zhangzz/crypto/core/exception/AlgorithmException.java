package com.zhangzz.crypto.core.exception;

/**
 * 算法异常
 */
public class AlgorithmException extends RuntimeException{

    private static final long serialVersionUID = 3L;

    public AlgorithmException(String msg, Throwable e) {
        super(msg, e);
    };

    public AlgorithmException(String msg) {
        super(msg);
    };
}
