package com.zhangzz.crypto.core.asn1;

import com.zhangzz.crypto.core.exception.CommonException;
import com.zhangzz.crypto.enums.Algorithm;
import com.zhangzz.crypto.enums.Padding;
import com.zhangzz.crypto.utils.ByteUtil;
import com.zhangzz.crypto.utils.CryptoUtil;
import org.bouncycastle.asn1.*;
import org.bouncycastle.crypto.engines.SM2Engine;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.math.ec.custom.gm.SM2P256V1Curve;

import java.io.IOException;
import java.math.BigInteger;

/**
 * SM2密文结构
 * @author zhangzz
 *
 */
public class SM2Cipher extends ASN1Object {

	//公钥的X值
	private ASN1Integer x;
	//公钥的Y值
	private ASN1Integer y;
	//加密明文的摘要值
	private ASN1OctetString hash;
	//加密后的密文
	private ASN1OctetString cipherText;
	//密文长度
	private ASN1Integer L;
	
	
	public static SM2Cipher getInstance(Object obj) {
		if(obj instanceof SM2Cipher) {
			return (SM2Cipher)obj;
		}else if(obj != null) {
			return new SM2Cipher(ASN1Sequence.getInstance(obj));
		}
		throw new CommonException("非空的ASN1对象或DER编码的字节数组");
	}
	
	public SM2Cipher(ASN1Sequence seq) {
		this.x = (ASN1Integer)seq.getObjectAt(0);
		this.y = (ASN1Integer)seq.getObjectAt(1);
		this.hash = ASN1OctetString.getInstance(seq.getObjectAt(2));
		this.cipherText = ASN1OctetString.getInstance(seq.getObjectAt(3));
		this.L = (ASN1Integer)seq.getObjectAt(4);
	}
	
	public SM2Cipher(byte[] x, byte[] y, byte[] hash, byte[] cipher, int l) {
		this.x = new ASN1Integer(new BigInteger(x));
		this.y = new ASN1Integer(new BigInteger(y));
		this.hash = new DEROctetString(hash);
		this.cipherText = new DEROctetString(cipher);
		this.L = new ASN1Integer(l);
	}

	public BigInteger getX() {
		return this.x.getValue();
	}
	
	public BigInteger getY() {
		return this.y.getValue();
	}
	
	public ASN1OctetString getHash() {
		return this.hash;
	}
	
	public ASN1OctetString getCipher() {
		return this.cipherText;
	}
	
	public int getL() {
		return this.L.getValue().intValue();
	}
	
	@Override
	public ASN1Primitive toASN1Primitive() {
		ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(x);
        v.add(y);
        v.add(hash);
        v.add(cipherText);
        v.add(L);
        return new DERSequence(v);
	}
}
